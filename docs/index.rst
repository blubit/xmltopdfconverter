pyxml2pdf - Creating PDF tables from XML
========================================

Convert XML input to PDF table. Since we forked the `upstream <https://github
.com/kuras120/XMLToPDFConverter>`_ this project has generalized quite a bit on the
generation of a multipage PDF file containing a table with subtables each containing
a subset of the xml tags based on the texts of some of their children tags. Since we
work on this project every end of the year, we will provide an extensive bit of
documentation by the end of November 2019.

For the *pyxml2pdf* homepage go to `GitHub <https://github.com/BjoernLudwigPTB/
XMLToPDFConverter>`_.

*pyxml2pdf* is written in Python 3.

Contents:

.. toctree::
    :maxdepth: 2

    pyxml2pdf.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
